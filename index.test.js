const faker = require('faker'); //this preset doesn't allow import
const lead = {
  name: faker.name.firstName(),
  email: faker.internet.email(),
  phone: faker.phone.phoneNumber(),
  message: faker.random.words()
};

describe('Contact form', () => {
    beforeAll(async () => {
        await page.goto('https://kodaktor.ru/g/puppetform');
    }, 30000);

    it('allows lead to submit a contact request', async () => {
        await page.waitForSelector('[data-test=contact-form]');
        await page.click('input[name=name]');
        await page.type('input[name=name]', lead.name);
        await page.click('input[name=email]');
        await page.type('input[name=email]', lead.email);
        await page.click('input[name=tel]');
        await page.type('input[name=tel]', lead.phone);
        await page.click('textarea[name=message]');
        await page.type('textarea[name=message]', lead.message);
        await page.click('input[type=checkbox]');
        await page.click('button[type=submit]');
        await page.waitForSelector('.modal');
    }, 30000);
});
